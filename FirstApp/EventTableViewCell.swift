//
//  EventTableViewCell.swift
//  FirstApp
//
//  Created by Duy Do on 10/31/17.
//  Copyright © 2017 Duy Do. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {

    @IBOutlet weak var eventCover: UIImageView!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var venue: UILabel!
    @IBOutlet weak var title: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    @IBAction func joinEvent(_ sender: UIButton) {
    }
}
