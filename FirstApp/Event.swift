////
////  Event.swift
////  FirstApp
////
////  Created by Duy Do on 10/31/17.
////  Copyright © 2017 Duy Do. All rights reserved.
////
//
//import UIKit
//import ObjectMapper
//
//class Event: Mappable {
//    // MARK: properties
//    var id: Int?
//    var title: String?
//    var startTime: Double?
//    var venue: String?
//    var coverImage: String?
//
//    required init?(map: Map) {
//
//    }
//
//    func mapping(map: Map) {
//        id <- map["id"]
//        title <- map["title"]
//        startTime <- map["startTime"]
//        venue <- map["venue"]
//        coverImage <- map["coverImage"]
//    }
//}

